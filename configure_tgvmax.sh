#!/bin/bash

echo ----------------------------------
echo WELCOME in the TGVmax configurator
echo Please answers these questions:
read -p 'Start city (ex: Bordeaux): ' city_start
read -p 'End city (ex: Paris): ' city_end
read -p 'Travel month: ' T_month
read -p 'Travel day: ' T_day
echo Looking for a $city_start to $city_end on the $T_day / $T_month
echo 'Email send to ?'
read -p 'Email send to : ' email_user1
echo 'Is there another email to add in Cc? '
read -p 'Email send to Cc:' email_user2
echo 'What is the GMAIL login to send mail?'
read -p 'Email:' email_owner
read -p 'Password:' password_owner
echo ----------------------------------

# Define Path
RUN_dir='TGVmax_running/'
mkdir -p $RUN_dir
mkdir -p TGVmax_journal
# Define travel city short name
first_cityS=${city_start::1}
first_cityE=${city_end::1}
init=${first_cityS}${first_cityE}
# Copy initial script to working script
FILEname='script_ouisncf_bot_'${init}${T_day}'_'${T_month}'.py'
if [ ! -f ${RUN_dir}$FILEname ]; then
 cp script_ouisncf_bot_initial.py ${RUN_dir}$FILEname
 [ ! -f ${RUN_dir}$FILEname ] && echo "[ERROR] script not created"
else
 echo 'This TGVmax already exist, run it again manualy with command in' ${RUN_dir}
 echo '    python' ${RUN_dir}$FILEname '& '
fi
# Set ARG in the new script
sed -i'' -e "s/START_city/$city_start/g" ${RUN_dir}$FILEname
sed -i'' -e "s/END_city/$city_end/g" ${RUN_dir}$FILEname
sed -i'' -e "s/DD_travel/$T_day/g" ${RUN_dir}$FILEname
sed -i'' -e "s/MM_travel/$T_month/g" ${RUN_dir}$FILEname
sed -i'' -e "s/INIT_travel/$init/g" ${RUN_dir}$FILEname
sed -i'' -e "s/EMAIL_1/$email_user1/g" ${RUN_dir}$FILEname
if [ ${#email_user2} -gt 5 ]; then 
 sed -i'' -e "s/EMAIL_2/$email_user2/g" ${RUN_dir}$FILEname
fi
sed -i'' -e "s/EMAIL_OWNER/$email_owner/g" ${RUN_dir}$FILEname
sed -i'' -e "s/EMAIL_PASSWORD/$password_owner/g" ${RUN_dir}$FILEname
# Get the IP without using TOR
MYIPis=`curl -s ifconfig.me`
sed -i'' -e "s/MYIP/$MYIPis/g" ${RUN_dir}$FILEname

echo '---> New TGVmax configured !!'
echo ----------------------------------
echo  NOW script is running !
echo ----------------------------------
cd ${RUN_dir}
python $FILEname 
