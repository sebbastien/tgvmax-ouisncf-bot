#!/bin/bash
if pgrep -x tor; then 
    echo 'Stepout: I kill TOR' 
    kill $(pgrep -x tor)
else
   echo 'Stepout: TOR is not found'
fi