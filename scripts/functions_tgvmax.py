
## Fonction
def check_running_page():
    ''' Check if oui.sncf/bot is well loaded '''
    test_ip = True
    try:
       driver.find_element(By.XPATH, "//*[@class='dot']")
       test_ip = False
    except :
       print('       :-) No ... on the page')
    if test_ip == False : 
       print('      ERROR loading bot: Restart python')
       print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
       proxies = {'http': "socks5://127.0.0.1:9050"}
       TestIP=(requests.get("http://httpbin.org/ip",proxies=proxies).text).replace('{\n  "origin": "','').replace('"\n}\n','')
       file = open('bad_IP_list.txt','a')
       file.write(str(datetime.now())[0:16]+'==> IP : '+TestIP+'    \n')
       file.close()
       driver.save_screenshot("screenshot_runningpage.png")
       driver.quit()
       os.system('python %s' % (filepy,) )
       exit()
    return

def run_and_click_ouisncf(url):
    ''' Check if oui.sncf/bot is well loaded '''
    driver.get(url)
    driver.implicitly_wait(15) # seconds
    # Activate the chat
    driver.find_element(By.XPATH, "//*[@id='cookie-policy-close']").click()
    driver.find_element(By.CSS_SELECTOR, ".cta").click()
    time.sleep(10)
    return

def send_message_ouisncf(message):
    '''  send the message to the chat ''' 
    From_id = driver.find_element(By.CSS_SELECTOR, '.input')
    From_id.clear()
    From_id.send_keys(message)
    driver.find_element(By.CSS_SELECTOR, '.input').send_keys(Keys.ENTER)
    time.sleep(10)
    return

def open_firefox():
    ''' Open Firefox with specific profile '''
    from selenium.webdriver.firefox.options import Options
    Options = Options()
    Options.headless = True
    ligne_al = N.random.randint(1,2100)
    user_agent = linecache.getline('scripts/Firefox.txt',ligne_al)
    profile = webdriver.FirefoxProfile()
    profile.set_preference("browser.cache.disk.enable", False)
    profile.set_preference("browser.cache.memory.enable", False)
    profile.set_preference("browser.cache.offline.enable", False)
    profile.set_preference("network.http.use-cache", False) 
    profile.set_preference('network.proxy.type', 1)
    profile.set_preference('network.proxy.socks', '127.0.0.1')
    profile.set_preference('network.proxy.socks_port', 9050)
    profile.set_preference('general.useragent.override', user_agent.rstrip('\n'))
    driver = webdriver.Firefox(firefox_profile=profile,options=Options)
    #driver = webdriver.Firefox(firefox_profile=profile)
    return driver

def get_status(driver):
    try:
        driver.execute(Command.STATUS)
        return "Firefox is still open"
    except (socket.error, httplib.CannotSendRequest):
        return "firefox is Dead"

def run_and_check_tor(MyIP):
    ''' run tor '''
    os.system('scripts/running_tor_multi.sh')
    time.sleep(10)
    # Select a new user_agent
    driver = open_firefox()
    # Directing the driver to the defined url
    url="https://ipleak.net/"
    driver.get(url)
    time.sleep(10)
    try:
        IP_check=driver.find_element(By.XPATH, "/html/body/div/div[3]/div[2]/div[1]/div[1]/a").text
        country_check=driver.find_element(By.XPATH, "/html/body/div/div[3]/div[2]/div[1]/div[1]/div[1]").text
    except :
        print('       Oups the page ipleak is lost .... Restart python')
        print('++++++++++++++++++++++')
        driver.save_screenshot("screenshot_runandchecktor.png")
        driver.quit()
        os.system('python %s' % (filepy,) )
        quit()
    if (str(IP_check[0:10])==MyIP[0:10]) or (str(country_check).lower() == 'france') :
        print(IP_check,country_check)
        print('Step1: Wrong IP... Restart python')
        print('++++++++++++++++++++++')
        driver.quit()
        os.system('python %s' % (filepy,) )
        quit()
    else: 
     print("Step1: IP:",str(IP_check), "Country:",str(country_check))
     driver.quit()
    return IP_check
