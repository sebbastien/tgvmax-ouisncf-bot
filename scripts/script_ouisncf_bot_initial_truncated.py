#!/usr/bin/env python
from __future__ import division
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.remote.command import Command
from bs4 import BeautifulSoup
from selenium.webdriver.common.keys import Keys
import pandas as pd
import numpy as N
import time
import os
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib
from datetime import datetime
import linecache
import requests

# Load functions
exec(open('scripts/functions_tgvmax.py').read())

# Configure path for 
dayv='DD_travel'
monthv='MM_travel'
arrive='END_city'
depart='START_city'
initial='INIT_travel'
message = '%s %s %s/%s tgvmax' % (depart,arrive,dayv,monthv,)
file_journal = 'journal_%s_%s%s.txt' % (initial,dayv,monthv,)
fromaddr = "EMAIL_OWNER"
tocc = "EMAIL_1"
toaddr = "EMAIL_2"
subj = "[TGVmax-bot] %s=>%s: %s/%s" % (depart,arrive,dayv,monthv,)
filepy = "script_ouisncf_bot_%s%s_%s.py" % (initial,dayv,monthv,)
myIPis = "MYIP"


print('++++++++++++++++++++++')
print("I'm starting",filepy,' (',time.ctime(),')')

## Run tor and check if OK
IP_check = run_and_check_tor(myIPis)

# Declare some variables
len_text_sup = 39
lignes = 2
count=-1
while count < 15 :
     ## Test IP
     print('To protect this work and avoid large usage of the script,')
     print('please ask to the owner these missing lines ...')
     ##
     if count == -1 :
      list_hdep_train_old = []
      ready = 'NO'
      error = 0
      ## Open firefox
      driver=open_firefox()
      ## Load Google
      driver.get('https://www.google.fr')
      driver.implicitly_wait(5) # seconds
      time.sleep(10)
      url = 'https://www.oui.sncf/bot'
      ## Load SNCF
      run_and_click_ouisncf(url)
      check_running_page()
     add_lines= 3
     ## Send message
     print('To protect this work and avoid large usage of the script,')
     print('please ask to the owner these missing lines ...')
     try:
            print('To protect this work and avoid large usage of the script,')
            print('please ask to the owner these missing lines ...')
     except :
       print('       Oups the page ouibot is lost .... Restart python')
       print('++++++++++++++++++++++')
       driver.save_screenshot("screenshot.png")
       driver.quit()
       os.system('python %s' % (filepy,) )
       quit()
     while ready == 'NO' :
         try:
            print('To protect this work and avoid large usage of the script,')
            print('please ask to the owner these missing lines ...')
         except :
            print('To protect this work and avoid large usage of the script,')
            print('please ask to the owner these missing lines ...')
         if error > 10:
            print( 'ERROR: run new script, error=',error)
            driver.save_screenshot("screenshot_error11.png")
            driver.quit()
            os.system('python %s' % (filepy,) )
            exit()
     print( 'Step4: Im in ligne=',lignes+add_lines,' (',time.ctime(),')')
     print( '       ',test_lignes)
     error = 0
     ready = 'NO'
     if ligne_new == 'TGVMax' :
            print('To protect this work and avoid large usage of the script,')
            print('please ask to the owner these missing lines ...')
            mail = 'YES'
     else :
            mail = 'NO' 
     print( 'step5: I send a mail? ',mail)
     file = open('TGVmax_journal/%s' % (file_journal,) ,'a')
     file.write(str(datetime.now())[0:16]+'==> Dispo : '+str(list_hdep_train_old)+'  ('+mail+') + '+TestIP+'    \n')
     file.close()
     count=count+1
     if mail == 'YES' :
            # send a mail 
            print('To protect this work and avoid large usage of the script,')
            print('please ask to the owner these missing lines ...')
     print('--------------------')
     time_rand = N.random.randint(200,1000)
     print('Step2: Wait for',time_rand,'s', ' at round',count,' IP is:',TestIP)
     time.sleep(time_rand)
else :
        os.system('scripts/kill_tor.sh')
        os.system('python %s' % (filepy,) )
        quit()
