
# TGVmax OUIbot

Dial with SNCF OUI bot to check TGVmax availability. Receive mail when a place becomes available.

**To protect this work, and to avoid a large use of this work which will make lose the advantage of it, some lines were removed from the code**

If you want to run this programme, please contact the owner.

# Use

Run configure_tgvmax.sh:
    Answer all the questions to get python script adapted with the corresponding date and place
    

# Detail

This programme combines bash and python scripts:  
- Bash scripts: 
    - [configure_tgvmax.sh] Automatic creation and run of python script from user queries.
    - [running_tor_init.sh] Check if tor is running. If not running then start it to get new IP adress.
- Python scripts:
    - [scrip_ouisncf_bot_initial.py] Inital python script with variables name that must be specify using configure_tgvmax.sh

# Description

The TGVmax card allows unlimited travel by train in France. The limit is the restricted number of available TGVmax seats in each train.
The booking starts 31 days in advance. Travellers must confirm their travail 48h before, otherwise, the booking is cancelled.

There are a very limited amount of seats open to the reservation, and new seats can be added during the 31 days. When looking for a seat it is needed to often check the availability on the chosen date and destination. The time spent for manually looking for a seat increase with the number of the planned journey.

This programme does the job automatically. By Chatting with the bot from oui.sncf.fr the programme asks as often as requested if there is a new seat available. If a seat becomes free, an email is sent to alert the user.    

# Disclaimer

Scrapping is not allowed on oui.sncf.fr. Use this programme at your own risk. 

